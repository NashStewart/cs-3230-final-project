package client;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * Handles all the images used in the GUI for the Stratego Game
 * 
 * @author Nash Stewart
 *
 */
public class Images 
{
	
	private BufferedImage imgBackgroundBoard;
	private BufferedImage imgBackgroundSide;
	
	private ImageIcon imgEnemyBack;
	private ImageIcon imgEnemyOne;
	private ImageIcon imgEnemyTwo;
	private ImageIcon imgEnemyThree;
	private ImageIcon imgEnemyFour;
	private ImageIcon imgEnemySpy;
	private ImageIcon imgEnemyBomb;
	private ImageIcon imgEnemyFlag;
	
	private ImageIcon imgBack;
	private ImageIcon imgOne;
	private ImageIcon imgTwo;
	private ImageIcon imgThree;
	private ImageIcon imgFour;
	private ImageIcon imgSpy;
	private ImageIcon imgBomb;
	private ImageIcon imgFlag;
	
	
	/**
	 * CONSTRUCTOR
	 * 
	 * Load all the images for the game
	 */
	public Images() 
	{
		try {
			imgBackgroundBoard = ImageIO.read(ResourceLoader.load(Constants.BACKGROUND_BOARD));
			imgBackgroundSide = ImageIO.read(ResourceLoader.load(Constants.BACKGROUND_SIDE));
			
			imgEnemyBack = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_BACK)));
			imgEnemyOne = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_ONE)));
			imgEnemyTwo = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_TWO)));
			imgEnemyThree = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_THREE)));
			imgEnemyFour = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_FOUR)));
			imgEnemySpy = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_SPY)));
			imgEnemyBomb = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_BOMB)));
			imgEnemyFlag = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ENEMY_FLAG)));
			
			imgBack = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_BACK)));
			imgOne = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_ONE)));
			imgTwo = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_TWO)));
			imgThree = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_THREE)));
			imgFour = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_FOUR)));
			imgSpy = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_SPY)));
			imgBomb = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_BOMB)));
			imgFlag = new ImageIcon(ImageIO.read(ResourceLoader.load(Constants.ICON_FLAG)));	
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
		}
	}


	/**
	 * @return Image for board background
	 */
	public BufferedImage getImgBackgroundBoard() 
	{
		return imgBackgroundBoard;
	}

	
	/**
	 * @return Image for side panel background
	 */
	public BufferedImage getImgBackgroundSide() 
	{
		return imgBackgroundSide;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the back of the piece
	 */
	public ImageIcon getImgBack(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyBack;
		}
		return imgBack;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the ONE piece
	 */
	public ImageIcon getImgOne(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyOne;
		}
		return imgOne;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the TWO piece
	 */
	public ImageIcon getImgTwo(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyTwo;
		}
		return imgTwo;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the THREE pieced
	 */
	public ImageIcon getImgThree(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyThree;
		}
		return imgThree;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the FOUR piece
	 */
	public ImageIcon getImgFour(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyFour;
		}
		return imgFour;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the SPY piece
	 */
	public ImageIcon getImgSpy(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemySpy;
		}
		return imgSpy;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the BOMB piece
	 */
	public ImageIcon getImgBomb(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyBomb;
		}
		return imgBomb;
	}

	
	/**
	 * @param isEnemy Is the piece owned by an enemy?
	 * @return Image for the FLAG piece
	 */
	public ImageIcon getImgFlag(Boolean isEnemy) 
	{
		if(isEnemy)
		{
			return imgEnemyFlag;
		}
		return imgFlag;
	}
	
	
}
