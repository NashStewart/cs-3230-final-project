package client;


/**
 * All possible game pieces.
 *
 * @author Nash Stewart
 *
 */
public enum Pieces 
{
	
	BOMB(-1, "Bomb"),
	EMPTY(0, ""),
	ONE(1, "1"),
	TWO(2, "2"),
	THREE(3, "3"),
	FOUR(4, "4"),
	SPY(5, "Spy"),
	FLAG(6, "Flag");
	
	private int value;
	private String text;
	
	
	/**
	 * CONSTRUCTOR
	 * 
	 * @param value numerical value of piece
	 * @param text text to display in reference to piece
	 */
	Pieces(int value, String text)
	{
		this.value = value;
		this.text = text;
	}
	
	
	/**
	 * @return numerical value of piece
	 */
	public int value()
	{
		return value;
	}
	
	
	/**
	 * @return textual reference to piece
	 */
	public String text()
	{
		return text;
	}
}
