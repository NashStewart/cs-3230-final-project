package client;

import java.io.InputStream;

/**
 * Helper which takes a path and returns an InputStream
 * 
 * @author Nash Stewart
 *
 */
final public class ResourceLoader 
{
	public static InputStream load(String path)
	{
		InputStream input = ResourceLoader.class.getResourceAsStream(path);
		if(input == null)
		{
			input = ResourceLoader.class.getResourceAsStream("/" + path);
		}
		return input;
	}
}
