package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Root window which holds all the components 
 * of the Stratego GUI
 * 
 * @author Nash Stewart
 *
 */
public class RootWindow extends JFrame
{
	
	static final long serialVersionUID = 3562402912451512043L;
	private InGameWindow gamePanel;
	private SidePanel sidePanel;
	Board board;
	
	/**
	 * CONSTRUCTOR
	 */
	public RootWindow(Board board) 
	{
		super();
		this.board = board;
		
		gamePanel = new InGameWindow(board);
		add(gamePanel, BorderLayout.CENTER);
		
		sidePanel = new SidePanel(board);
		add(sidePanel, BorderLayout.EAST);
		
		pack();
		
		setTitle("Stratego");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
	}

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try {
					RootWindow frame = new RootWindow(new Board());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
