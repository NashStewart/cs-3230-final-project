package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Panel for the Chat components and the pieces
 * available during game setup
 * 
 * @author Nash Stewart
 *
 */
public class SidePanel extends JPanel 
{

	private static final long serialVersionUID = -2974508659846288845L;
	
	private Images images;
	private JLabel[] pieces;
	private int[] piecesValues;
	private Board board;
	private int selectedPiece;
	private int selectedPieceValue;
	private Border selectedBorder;
	private Border unselectedBorder;
	
	
	/**
	 * CONSTRUCTOR
	 */
	public SidePanel(Board board) 
	{
		super();
		this.board = board;
		this.images = new Images();
		this.pieces = new JLabel[Constants.NUMBER_OF_UNIQUE_PIECES];
		this.piecesValues = new int[Constants.NUMBER_OF_UNIQUE_PIECES];
		this.selectedPiece = 0;
		this.selectedPieceValue = 0;
		this.selectedBorder = BorderFactory.createLineBorder(Color.WHITE);
		this.unselectedBorder = BorderFactory.createLineBorder(new Color(25, 25, 55));
		
		// Register values with JLabel representation of piece
		piecesValues[0] = 0;
		piecesValues[1] = Pieces.FLAG.value();
		piecesValues[2] = Pieces.ONE.value();
		piecesValues[3] = Pieces.TWO.value();
		piecesValues[4] = Pieces.THREE.value();
		piecesValues[5] = Pieces.FOUR.value();
		piecesValues[6] = Pieces.SPY.value();
		piecesValues[7] = Pieces.BOMB.value();
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 15));
		setPreferredSize(new Dimension(220, 650));
		setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.LIGHT_GRAY));
		
		createChat();
		
		JButton btnReady = new JButton("Ready");
		add(btnReady);
		
		createPieces(board);
	}


	/**
	 * Add JLabels representing all the available pieces to 
	 * place on bord during game setup
	 * 
	 * @param board
	 */
	private void createPieces(Board board) 
	{
		// Add array of all pieces
		for(int i = 0; i < pieces.length; i++)
		{
			final int pieceIndex = i;
			
			pieces[pieceIndex] = new JLabel();
			pieces[pieceIndex].setHorizontalTextPosition(JLabel.LEFT);
			pieces[pieceIndex].setVerticalTextPosition(JLabel.CENTER);
			pieces[pieceIndex].setHorizontalAlignment(JLabel.CENTER);
			pieces[pieceIndex].setVerticalAlignment(JLabel.CENTER);
			pieces[pieceIndex].setForeground(Color.WHITE);
			pieces[pieceIndex].setBackground(new Color(25, 25, 55));
			pieces[pieceIndex].setOpaque(true);
			pieces[pieceIndex].setBorder(unselectedBorder);
			pieces[pieceIndex].addMouseListener(new MouseAdapter()  
			{  
			    public void mouseClicked(MouseEvent e)  
			    {  
			    	deselectPiece();
			    	selectPiece(pieceIndex);
			    	int value = getSelectedPieceValue();
			    	board.setSelectedPieceValue(value);
			    	((JLabel) e.getSource()).setText("_");
			    }  
			});
			
			add(pieces[pieceIndex]);
		}
		
		// Add images representing each piece available to place
		pieces[0].setIcon(images.getImgBack(false));
		pieces[1].setIcon(images.getImgFlag(false));
		pieces[2].setIcon(images.getImgOne(false));
		pieces[3].setIcon(images.getImgTwo(false));
		pieces[4].setIcon(images.getImgThree(false));
		pieces[5].setIcon(images.getImgFour(false));
		pieces[6].setIcon(images.getImgSpy(false));
		pieces[7].setIcon(images.getImgBomb(false));
		
		updatePiecesLeft();
		
		// Set tool tip text to reflect the type of piece
		pieces[0].setToolTipText("Total Pieces Left");
		pieces[1].setToolTipText(Pieces.FLAG.text());
		pieces[2].setToolTipText(Pieces.ONE.text());
		pieces[3].setToolTipText(Pieces.TWO.text());
		pieces[4].setToolTipText(Pieces.THREE.text());
		pieces[5].setToolTipText(Pieces.FOUR.text());
		pieces[6].setToolTipText(Pieces.SPY.text());
		pieces[7].setToolTipText(Pieces.BOMB.text());
	}


	/**
	 * Add game chat to panel
	 */
	private void createChat() {
		JTextArea txtChat = new JTextArea("Connected to chat client.", 10, 16);
		txtChat.setMargin( new Insets(2,2,2,2) );
		txtChat.setLineWrap(true);
		txtChat.setEditable(false);
		txtChat.setBackground(new Color(25, 25, 55));
		txtChat.setForeground(new Color(50, 175, 225));
		JScrollPane scrollPaneChat = new JScrollPane(txtChat);
		
		JTextField entryChat = new JTextField(16);
		entryChat.setBackground(new Color(25, 25, 55));
		entryChat.setForeground(new Color(50, 175, 225));
		entryChat.setMargin( new Insets(2,2,2,2) );
		
		JButton btnSend = new JButton("Send");
		
		add(scrollPaneChat);
		add(entryChat);
		add(btnSend);
	}


	/**
	 * Update the text on the pieces JLabels to reflect the actual
	 * number of each piece left to place on board during game setup.
	 */
	public void updatePiecesLeft() 
	{
		
		pieces[0].setText(Integer.toString(board.getTotalOffBoard()));
		pieces[1].setText(Integer.toString(board.getNumberOffBoard(Pieces.FLAG)));
		pieces[2].setText(Integer.toString(board.getNumberOffBoard(Pieces.ONE)));
		pieces[3].setText(Integer.toString(board.getNumberOffBoard(Pieces.TWO)));
		pieces[4].setText(Integer.toString(board.getNumberOffBoard(Pieces.THREE)));
		pieces[5].setText(Integer.toString(board.getNumberOffBoard(Pieces.FOUR)));
		pieces[6].setText(Integer.toString(board.getNumberOffBoard(Pieces.SPY)));
		pieces[7].setText(Integer.toString(board.getNumberOffBoard(Pieces.BOMB)));
		repaint();
		revalidate();
	}
	
	
	/**
	 * Select a piece and change the JLabel border to reflect the selection.
	 * 
	 * @param piece index of JLabel representing the piece being selected
	 */
	private void selectPiece(int piece)
	{
		
		selectedPiece = piece;
		selectedPieceValue = piecesValues[piece];
		if(piece != 0)
		{
			pieces[selectedPiece].setBorder(selectedBorder); 
		}
		revalidate();
	}
	
	
	/**
	 * 
	 * @param piece
	 */
	private void deselectPiece()
	{
		pieces[selectedPiece].setBorder(unselectedBorder); 
		updatePiecesLeft();
	}
	
	
	/**
	 * @return Currently selected piece value
	 */
	private int getSelectedPieceValue() 
	{
		return selectedPieceValue;
	}


	/**
	 * Add background
	 */
	@Override
	protected void paintComponent(Graphics g) 
	{
	    super.paintComponent(g);
	    int x = (this.getWidth() - images.getImgBackgroundSide().getWidth(null)) / 2;
	    int y = -200;
	    g.drawImage(images.getImgBackgroundSide(), x, y, null);
	}
}
