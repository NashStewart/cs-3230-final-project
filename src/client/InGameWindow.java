package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * Panel for the Stratego game board
 * 
 * @author Nash Stewart
 *
 */
public class InGameWindow extends JPanel 
{

	private static final long serialVersionUID = -2974508659846288845L;
	private JLabel[][] tiles;
	private Images images;
	private Board board;
	
	
	/**
	 * CONSTRUCTOR
	 */
	public InGameWindow(Board board) 
	{
		super();
		this.board = board;
		this.images = new Images();
		
		// Set Layout
		GridLayout gridLayout = new GridLayout();
		gridLayout.setColumns(6);
		gridLayout.setHgap(2);
		gridLayout.setVgap(2);
		gridLayout.setRows(6);
		setLayout(gridLayout);
		
		setPreferredSize(new Dimension(650, 650));
		
		// Create array of tiles and border for them
		this.tiles = new JLabel[Constants.BOARD_ROWS][Constants.BOARD_COLUMNS];
		Border tileBorder = BorderFactory.createLineBorder(Color.GRAY);
		
		// Add array of tiles equal to the sizes set in Constants
		for(int i = 0; i < Constants.BOARD_ROWS; i++)
		{
			for(int j = 0; j < Constants.BOARD_COLUMNS; j++)
			{
				int row = i;
				int column = j;
				
				tiles[row][column] = new JLabel();
				tiles[row][column].setHorizontalTextPosition(JLabel.CENTER);
				tiles[row][column].setVerticalTextPosition(JLabel.BOTTOM);
				tiles[row][column].setHorizontalAlignment(JLabel.CENTER);
				tiles[row][column].setVerticalAlignment(JLabel.CENTER);
				tiles[row][column].setForeground(Color.WHITE);
				tiles[row][column].setBorder(tileBorder);
				tiles[row][column].addMouseListener(new MouseAdapter()  
				{  
				    public void mouseClicked(MouseEvent e)  
				    {  
				    	// Add piece with single click
				    	if(board.getValue(row, column) == 0 && row > 3 && 
				    			board.getNumberOffBoard(getPieceByValue(board.getSelectedPieceValue())) > 0)
				    	{
					    	board.putOnBoard(row, column, board.getSelectedPieceValue());
					    	((JLabel) e.getSource()).setIcon(getImageByValue(board.getSelectedPieceValue()));
				    	}
				    	
				    	// Remove piece with double click
				    	if (e.getClickCount() == 2) 
				    	{
				    		board.removeFromBoard(row, column);
				    		((JLabel) e.getSource()).setIcon(null);
				    	}
				    	
				    	repaint();
				    	revalidate();
				    }  
				});
				
				// Add Sith icons to enemy side
				if(row < 2) 
				{
					tiles[row][column].setIcon(images.getImgBack(true));
				}
				
				add(tiles[row][column]);
			}
		}	
	}
	
	
	/**
	 * @param value value of a piece
	 * @return Image for specified piece
	 */
	private ImageIcon getImageByValue(int value)
	{
		switch(board.getSelectedPieceValue())
		{
			case -1:
				return images.getImgBomb(false);
			case 1:
				return images.getImgOne(false);
			case 2:
				return images.getImgTwo(false);
			case 3:
				return images.getImgThree(false);
			case 4:
				return images.getImgFour(false);
			case 5:
				return images.getImgSpy(false);
			case 6:
				return images.getImgFlag(false);
			default:
				return null;
		}
	}
	
	
	/**
	 * @param value value of a piece
	 * @return Piece registered to specified value
	 */
	private Pieces getPieceByValue(int value)
	{
		switch(value)
		{
			case -1:
				return Pieces.BOMB;
			case 1:
				return Pieces.ONE;
			case 2:
				return Pieces.TWO;
			case 3:
				return Pieces.THREE;
			case 4:
				return Pieces.FOUR;
			case 5:
				return Pieces.SPY;
			case 6:
				return Pieces.FLAG;
			default:
				return null;
		}
	}
	
	
	/**
	 * Add background
	 */
	@Override
	protected void paintComponent(Graphics g) 
	{
	    super.paintComponent(g);
	    int x = (this.getWidth() - images.getImgBackgroundBoard().getWidth(null)) / 2;
	    int y = (this.getHeight() - images.getImgBackgroundBoard().getHeight(null)) / 2;
	    g.drawImage(images.getImgBackgroundBoard(), x, y, null);
	}
}
