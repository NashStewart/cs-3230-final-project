package client;

/**
 * Virtual representation of a Stratego game board and its pieces.
 * 
 * @author Nash Stewart
 *
 */
public class Board 
{
	
	private boolean isGameStarted;
	private int selectedPieceValue;
	
	private int[][] boardValues; // values of each piece on the board
	private boolean[][] isEnemy; // True = enemy owns the piece at specified location on the board.
	
	private int offBoardBombs;
	private int offBoardOnes;
	private int offBoardTwos;
	private int offBoardThrees;
	private int offBoardFours;
	private int offBoardSpys;
	private int offBoardFlags;
	
	/**
	 * CONSTRUCTOR
	 */
	public Board() 
	{
		this.isGameStarted = false;
		this.selectedPieceValue = 0;
		
		this.boardValues = new int[Constants.BOARD_ROWS][Constants.BOARD_COLUMNS];
		this.isEnemy = new boolean[Constants.BOARD_ROWS][Constants.BOARD_COLUMNS];
		
		this.offBoardBombs = 1;
		this.offBoardOnes = 1;
		this.offBoardTwos = 1;
		this.offBoardThrees = 3;
		this.offBoardFours = 4;
		this.offBoardSpys = 1;
		this.offBoardFlags = 1;
	}
	
	
	/**
	 * Update tile on game board with specified value
	 * 
	 * @param row row on game board
	 * @param column column on game board
	 * @param value new value of tile
	 */
	public void update(int row, int column, int value, boolean isEnemy)
	{
		if(row >= 0 && row < Constants.BOARD_ROWS)
		{
			if(column >= 0 && column < Constants.BOARD_COLUMNS)
			{
				this.boardValues[row][column] = value;
				this.isEnemy[row][column] = isEnemy;
			}
		}
	}
	
	
	/**
	 * Move piece and resolve conflicts with enemy piece is occupying specified tile.
	 * Also checks for win.
	 * 
	 * @param currentRow current row of piece to move
	 * @param currentColumn current column of piece to move
	 * @param newRow row to move piece to
	 * @param newColumn column to move piece to
	 */
	public void move(int currentRow, int currentColumn, int newRow, int newColumn)
	{
		if( (currentRow >= 0 && currentRow < Constants.BOARD_ROWS) 
				&& (newRow >= 0 && newRow < Constants.BOARD_ROWS))
		{
			if( (currentColumn >= 0 && currentColumn < Constants.BOARD_COLUMNS) 
					&& (newColumn >= 0 && newColumn < Constants.BOARD_COLUMNS))
			{
				if(getValue(newRow, newColumn) == 6)
				{
					// TODO add call to ServerInterface to send end game message
					System.out.print("YOU WIN!");
				}
				
				if(isEnemy[newRow][newColumn] || getValue(newRow, newColumn) == 0)
				{
					if(getValue(currentRow, currentColumn) > getValue(newRow, newColumn))
					{
						update(newRow, newColumn, getValue(currentRow, currentColumn), false);
						
					}
					
					if(getValue(currentRow, currentColumn) == getValue(newRow, newColumn)) 
					{
						update(newRow, newColumn, 0, false);
					} 
					
					update(currentRow, currentColumn, 0, false);
				}
			}
		}
	}
	
	
	/**
	 * @param row row of desired piece value
	 * @param column column of desired piece value
	 * @return value of the piece in the specified row and column
	 */
	public int getValue(int row, int column)
	{
		return this.boardValues[row][column];
	}
	
	
	/**
	 * @param piece the specified game piece
	 * @return the number of that type of piece that are not on the board
	 */
	public int getNumberOffBoard(Pieces piece)
	{
		switch(piece)
		{
			case BOMB:
				return this.offBoardBombs;
			case ONE:
				return this.offBoardOnes;
			case TWO:
				return this.offBoardTwos;
			case THREE:
				return this.offBoardThrees;
			case FOUR:
				return this.offBoardFours;
			case SPY:
				return this.offBoardSpys;
			case FLAG:
				return this.offBoardFlags;
			default:
				return 0;
		}
	}
	
	
	/**
	 * @return total number of friendly pieces off the board
	 */
	public int getTotalOffBoard()
	{
		return offBoardBombs + offBoardOnes + offBoardTwos + offBoardThrees +
				offBoardFours + offBoardSpys + offBoardFlags;
	}
	
	
	/**
	 * Remove a piece from the board
	 * 
	 * @param row row of piece
	 * @param column column of piece
	 * @param value value of piece
	 */
	public void putOnBoard(int row, int column, int value)
	{
		if(row >= 0 && row < Constants.BOARD_ROWS)
		{
			if(column >= 0 && column < Constants.BOARD_COLUMNS)
			{
				update(row, column, value, false);
				
				switch(value)
				{
					case -1:
						offBoardBombs--;
						break;
					case 1:
						offBoardOnes--;
						break;
					case 2:
						offBoardTwos--;
						break;
					case 3:
						offBoardThrees--;
						break;
					case 4:
						offBoardFours--;
						break;
					case 5:
						offBoardSpys--;
						break;
					case 6:
						offBoardFlags--;
						break;
					default:
						break;
				}
			}
		}
	}
	
	
	/**
	 * Set a space on the board to 0 reflecting an empty space
	 * and update how many of the piece that was removed are
	 * off the board now.
	 * 
	 * @param row row of piece to be removed
	 * @param column column of piece to be removed
	 */
	public void removeFromBoard(int row, int column)
	{
		if(row >= 0 && row < Constants.BOARD_ROWS)
		{
			if(column >= 0 && column < Constants.BOARD_COLUMNS)
			{	
				if(!isEnemy[row][column])
				{
					switch(boardValues[row][column])
					{
						case -1:
							offBoardBombs++;
							break;
						case 1:
							offBoardOnes++;
							break;
						case 2:
							offBoardTwos++;
							break;
						case 3:
							offBoardThrees++;
							break;
						case 4:
							offBoardFours++;
							break;
						case 5:
							offBoardSpys++;
							break;
						case 6:
							offBoardFlags++;
							break;
						default:
							break;
					}
				}
				
				update(row, column, Pieces.EMPTY.value(), false);
			}
		}
	}
	
	
	/**
	 * @return Has the game started?
	 */
	public boolean isGameStarted() 
	{
		return isGameStarted;
	}


	/**
	 * @param isGameStarted Staty if the game has started
	 */
	public void setGameStarted(boolean isGameStarted) 
	{
		this.isGameStarted = isGameStarted;
	}


	/**
	 * @return the currently selected piece
	 */
	public int getSelectedPieceValue() 
	{
		return selectedPieceValue;
	}


	/**
	 * Set which piece is currently selected
	 * 
	 * @param selectedPieceValue the new currently selected piece
	 */
	public void setSelectedPieceValue(int selectedPieceValue) 
	{
		this.selectedPieceValue = selectedPieceValue;
	}
}
