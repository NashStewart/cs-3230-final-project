package client;

/**
 * Constants used throughout the Stratego game and its GUI.
 * 
 * @author Nash Stewart
 * 
 */
public class Constants 
{
	public static final int TILE_WIDTH = 100;
	public static final int TILE_HEIGHT = 100;
	
	public static final int BOARD_ROWS = 6;
	public static final int BOARD_COLUMNS = 6;
	
	public static final int NUMBER_OF_UNIQUE_PIECES = 8;
	
	public static final String BACKGROUND_BOARD = "starwars.jpg";
	public static final String BACKGROUND_SIDE = "falcon.jpg";
	
	public static final String ICON_ENEMY_BACK = "sith.png";
	public static final String ICON_ENEMY_ONE = "vader.jpg";
	public static final String ICON_ENEMY_TWO = "sidius.jpg";
	public static final String ICON_ENEMY_THREE = "plagueis.jpg";
	public static final String ICON_ENEMY_FOUR = "bane.jpg";
	public static final String ICON_ENEMY_SPY = "kylo.jpg";
	public static final String ICON_ENEMY_BOMB = "sarlaccpit.jpg";
	public static final String ICON_ENEMY_FLAG = "deathstar.jpg";
	
	public static final String ICON_BACK = "jedi.jpg";
	public static final String ICON_ONE = "luke.jpg";
	public static final String ICON_TWO = "yoda.jpg";
	public static final String ICON_THREE = "windu.jpg";
	public static final String ICON_FOUR = "kenobi.jpg";
	public static final String ICON_SPY = "anakin.jpg";
	public static final String ICON_BOMB = "jarjar.jpg";
	public static final String ICON_FLAG = "jeditemple.jpg";
}
